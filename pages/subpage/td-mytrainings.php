<?php

$member = new member();
if(!$member->isConnected())
	$member->loginValidate();
else
	$member->getData();
$result = $member->getTrainings();

$ratingatc = array("None","AS1","AS2","AS3","ADC","APC","ACC","SEC","SAI","CAI");
$ratingpilot = array("None","FS1","FS2","FS3","PP","SPP","CP","ATP","SFI","CFI");


if($_SESSION['lang'] == "PT")
{
	$status = array("Solicitado","Em processo","Finalizado","Cancelado");
	$score = array("Apto","Inapto","Cancelado");
	$class_status =array("label label-warning","label label-info","label label-success","label label-important");
	$class_score = array("label label-success","label label-warning","label label-important");
?>


<div id="container" class="container-fluid">
	<div class="row-fluid">
		<div id="pagina" class="col-lg-12">
			<div class="card">
				<div class="card-header">
					<i class="mr-2 fa fa-align-justify"></i>
					<strong class="card-title" v-if="headerText">Histórico Treinamentos</strong>
				</div>
				 <div class="card-body table-responsive">
					<table class="table table-borderless table-striped table-earning">
					  <thead>
						<tr>
						  <th width="50">Tipo</th>
						  <th width="200">Instrutor</th>
						  <th width="100">Status</th>
						  <th width="100">Detalhes</th>
						</tr>
					  </thead>
					  <tbody>
						  <?php $i=0;foreach($result as $training) {?>
						  <tr>
							<td><?php echo $training['trainingType']; ?></td>
							<td><?php echo "<a target='_blank' href='https://www.ivao.aero/Member.aspx?id=".$training['instructor']."'>".strtoupper($training['instructor'])."</a>"?>
							<td><?php echo "<span class=\"".$class_status[$training['status']]."\">".$status[$training['status']]."</span>"; ?></td>
							<td><a href="#" onclick="<?php echo "openModal(".$training['id'].")"; ?>" class="btn btn-success">Ver Detalhes</a></td>
						  </tr>
						  <?php $i++;} ?>
					  </tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<?php $i=0;foreach($result as $training){
	
	?>
	<div id="<?php echo "myModal".$training['id']; ?>" style="display:none" class="modal fade show" id="scrollmodal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" style="display: block; padding-right: 17px;">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="scrollmodalLabel">Detalhes Treinamento</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span onclick="<?php echo "closeModal(".$training['id'].")"; ?>">×</span>
					</button>
				</div>
				<div class="modal- table-responsive">
					<table class="table table-borderless table-striped table-earning">
						<tr>
							<th>ID</th>
							<td><?php echo $training['id'];?></td>
						</tr>
						<tr>
							<th>Modalidade</th>
							<td><?php echo $training['trainingType'];?></td>
						</tr>
						<tr>
							<th>Status</th>
							<td><?php echo "<span class=\"".$class_status[$training['status']]."\">".$status[$training['status']]."</span>"; ?></td>
						</tr>
						<tr>
							<th>Data de Solicitação</th>
							<td><?php echo date("d/m/Y",$training['requestData']);?></td>
						</tr>
						<tr>
							<th>Data de Encerramento</th>
							<td><?php if($training['closeData'] != "")echo date("d/m/Y",$training['closeData']);?></td>
						</tr>
						<tr>
							<th>Avaliação</th>
							<td><?php echo "<span class=\"".$class_score[$training['result']]."\">".$score[$training['result']]."</span>"; ?></td>
						</tr>
						<tr>
							<th>Instrutor</th>
							<td><?php echo $training['instructor'];?></td>
						</tr>
						<tr>
							<th>Comentários Instrutor</th>
							<td>
							</td>
						</tr>
					</table>
					<hr>
					<div class="card-title">
						<h3 class="text-center title-2">Comentário Instrutor</h3>
					</div>
					<?php echo $training['rmk'];?>
				</div>
			</div>
		</div>
	</div>
	<?php $i++; } ?>

</div>

<?php } else if($_SESSION['lang'] == "EN"){ $status = array("Created","Evaluating","Finished","Canceled");
	$score = array("Able","Unable","Canceled");
	$class_status =array("label label-warning","label label-info","label label-success","label label-important");
	$class_score = array("label label-success","label label-warning","label label-important");
?>

<div id="container" class="container-fluid">
	<div class="row-fluid">
		<div id="pagina" class="col-lg-12">
			<div class="card">
				<div class="card-header">
					<i class="mr-2 fa fa-align-justify"></i>
					<strong class="card-title" v-if="headerText">Trainings History</strong>
				</div>
				 <div class="card-body table-responsive">
					<table class="table table-borderless table-striped table-earning">
					  <thead>
						<tr>
						  <th width="50">Type</th>
						  <th width="200">Instructor</th>
						  <th width="100">Status</th>
						  <th width="100">Details</th>
						</tr>
					  </thead>
					  <tbody>
						  <?php $i=0;foreach($result as $training) {?>
						  <tr>
							<td><?php echo $training['trainingType']; ?></td>
							<td><?php echo "<a target='_blank' href='https://www.ivao.aero/Member.aspx?id=".$training['instructor']."'>".strtoupper($training['instructor'])."</a>"?>
							<td><?php echo "<span class=\"".$class_status[$training['status']]."\">".$status[$training['status']]."</span>"; ?></td>
							<td><a href="#" onclick="<?php echo "openModal(".$training['id'].")"; ?>" class="btn btn-success">Details</a></td>
						  </tr>
						  <?php $i++;} ?>
					  </tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<?php $i=0;foreach($result as $training){
	?>
	<div id="<?php echo "myModal".$training['id']; ?>" style="display:none" class="modal fade show" id="scrollmodal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" style="display: block; padding-right: 17px;">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="scrollmodalLabel">Training Details</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span onclick="<?php echo "closeModal(".$training['id'].")"; ?>">×</span>
					</button>
				</div>
				<div class="modal-body table-responsive">
					<table class="table table-borderless table-striped table-earning">
						<tr>
							<th>ID</th>
							<td><?php echo $training['id'];?></td>
						</tr>
						<tr>
							<th>Type</th>
							<td><?php echo $training['trainingType'];?></td>
						</tr>
						<tr>
							<th>Status</th>
							<td><?php echo "<span class=\"".$class_status[$training['status']]."\">".$status[$training['status']]."</span>"; ?></td>
						</tr>
						<tr>
							<th>Request Data</th>
							<td><?php echo date("d/m/Y",$training['requestData']);?></td>
						</tr>
						<tr>
							<th>End Data</th>
							<td><?php if($training['closeData'] != "")echo date("d/m/Y",$training['closeData']);?></td>
						</tr>
						<tr>
							<th>Score</th>
							<td><?php echo "<span class=\"".$class_score[$training['result']]."\">".$score[$training['result']]."</span>"; ?></td>
						</tr>
						<tr>
							<th>Instructor</th>
							<td><?php echo $training['instructor'];?></td>
						</tr>
					</table>
					<hr>
					<div class="card-title">
						<h3 class="text-center title-2">Instructor Comments</h3>
					</div>
					<?php echo $training['rmk'];?>
				</div>
			</div>
		</div>
	</div>
	<?php $i++; } ?>

</div>

<?php } ?>
<style>
#container
{
	text-align: left;
}
td
{
	text-align: center;
}
</style>

<script>
function openModal(id)
{
	$
	(
		function ()
		{
			$("#myModal"+id).show();
		}
	);
}

function closeModal(id)
{
	$
	(
		function ()
		{
			$("#myModal"+id).hide();
		}
	);
}
</script>



