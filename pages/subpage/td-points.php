<?php


$member = new member();
if (!$member->isConnected()) {
    $member->loginValidate();
} else {
    $member->getData();
}

$division = new division();
$division->db = $member->db;

$result = $member->getLegsPS();
$repor = $member->getLegs(0);
$repor = $repor[0]['total'];
$apprv = $member->getLegs(1);
$apprv = $apprv[0]['total'];
$rejtd = $member->getLegs(2);
$rejtd = $rejtd[0]['total'];
$modif = $member->getLegs(3);
$modif = $modif[0]['total'];
$total = $repor + $apprv + $rejtd + $modif;
$schedule = $division->getTDBooks();

if ($_SESSION['lang'] == "PT") {
    $status = array("Reportado", "Aceito", "Rejeitado", "Requer modificação");
    $class_status = array("label label-info", "label label-success", "label label-important", "label label-warning");
    ?>
<style type="text/css">.center{ text-align: center; }</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<div id="botao-reportar" class="container-fluid">
	<div class="row">
		<div id="pagina" class="col-lg-12">
			<div class="card">
				<div class="card-header">
					<i class="mr-2 fa fa-align-justify"></i>
					<strong class="card-title" v-if="headerText">Reporte mais uma leg</strong>
				</div>
				<div class="card-body">
					<a href="#" class="btn btn-outline-success" onclick="botaoReportarLeg()" id="botaoReportarLeg">Reportar um voo</a>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="form-reportar" class="container-fluid">
	<div class="row">
		<div id="pagina" class="col-lg-12">
			<div class="card">
				<div class="widget-box">
					<div class="card-header">
						<i class="mr-2 fa fa-align-justify"></i>
						<strong class="card-title" v-if="headerText">Reporte mais uma leg</strong>
					</div>
					<div class="card-body">
						<div class="form-horizontal">
							<div class="row form-group">
								<div class="col col-md-3">
									<label for="hf-email" class=" form-control-label">VID</label>
								</div>
								<div class="col-12 col-md-9">
									<input id="vid" type="text" class="form-control" readonly="true" value="<?php echo $member->vid; ?>" />
								</div>
							</div>
							<div class="row form-group">
								<div class="col col-md-3">
									<label for="hf-email" class=" form-control-label">Callsign</label>
								</div>
								<div class="col-12 col-md-9">
									<input id="callsign" type="text"  class="form-control" placeholder="Insira seu callsign" maxlength="7" />
								</div>
							</div>
							<div class="row form-group">
								<div class="col col-md-3">
									<label for="hf-email" class=" form-control-label">Remarks</label>
								</div>
								<div class="col-12 col-md-9">
									<textarea id="rmk" type="text"  class="form-control" placeholder="Insira aqui qualquer remark que deseja incluir (Limitado a 250 caracteres)" maxlength="250" /></textarea>
								</div>
							</div>
							<div class="row form-group">
								<div class="col col-md-3">
									<label for="hf-email" class=" form-control-label">Data do voo</label>
								</div>
								<div class="col-12 col-md-9">
									<input id="date" type="date" class="form-control"/>
								</div>
							</div>
							<div class="row form-group">
								<div class="col col-md-3">
									<label for="hf-email" class=" form-control-label">Selecione o exame/treinamento</label>
								</div>
								<div class="col-12 col-md-9">
									<select id="schedule" class="form-control" required>
										<option selected disabled hidden>Selecione aqui o exame/treinamento</option>
										<?php $i = 0;foreach ($schedule as $event) {
        $fiveDays = date("U", strtotime('-15 days'));
        if (($event['rating'] == "ADC" || $event['rating'] == "APC" || $event['rating'] == "ACC" || $event['rating'] == "SEC") && ($fiveDays < $event['data']) && ($event['data'] <= date("U"))) {?>
										<option value="<?php echo $event['id']; ?>"><?php echo $event['rating'] . " " . $event['local'] . " " . date("d/m/Y", $event['data']) . " - Horário: " . $event['time'] . "Z"; ?></option>
										<?php }
        $i++;}?>
									</select>
								</div>
							</div>
							<div class="row form-group">
								<div class="col col-md-3">
									<label for="hf-email" class=" form-control-label">Política</label>
								</div>
								<div class="col-12 col-md-9">
									<span class="help-block">Ao clicar no botão <b>Enviar</b> você concorda com a seguinte <b><a onclick="showLegPolicy()" href="#">política</a></b></span><br>
								</div>
								<div class="col-12 col-md-9 controls" id="legPolicy">
									<hr>
									1. São elegíveis para obter a Pilot Support Award todos os membros da IVAO que acumularem pelo menos:<br>
									&nbsp;&nbsp;&nbsp;&nbsp;a. Bronze - 10 pontos;<br>
									&nbsp;&nbsp;&nbsp;&nbsp;b. Silver - 20 pontos;<br>
									&nbsp;&nbsp;&nbsp;&nbsp;c. Gold - 40 pontos;<br>
									&nbsp;&nbsp;&nbsp;&nbsp;d. Platinum - 60 pontos;<br>
									&nbsp;&nbsp;&nbsp;&nbsp;e. Diamond - 80 pontos.<hr>
									2. Os pontos poderão ser obtidos mediante participação como piloto em treinamentos e exames de ATC.<hr>
									3. Cada participação (treinamento ou exame) tem a validade de 1 ponto, sendo necessário o mínimo de:<br>
									&nbsp;&nbsp;&nbsp;&nbsp;I. Treinamento/Exame de ADC:<br>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a. VFR - 30 minutos de voo contados da hora de decolagem até a hora de pouso;<br>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b. IFR - 20 minutos de voo contados da hora de decolagem até a hora de pouso.<br>
									&nbsp;&nbsp;&nbsp;&nbsp;II. Treinamento/Exame de APC:<br>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a. VFR - 40 minutos de voo contados da hora de decolagem até a hora de pouso;<br>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b. IFR - 45 minutos de voo contados da hora de decolagem até a hora de pouso.<br>
									&nbsp;&nbsp;&nbsp;&nbsp;III. Treinamento/Exame de ACC:<br>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a. VFR - 45 minutos de voo contados da hora de decolagem até a hora de pouso;<br>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b. IFR - 60 minutos de voo contados da hora de decolagem até a hora de pouso.<br>
									&nbsp;&nbsp;&nbsp;&nbsp;IV. Treinamento/Exame de SEC:<br>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a. VFR - 40 minutos de voo contados da hora de decolagem até a hora de pouso;<br>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b. IFR - 45 minutos de voo contados da hora de decolagem até a hora de pouso.<hr>
									4. Só será contabilizado um ponto por treinamento ou exame.<hr>
									5. Os pontos obtidos são exclusivos da Divisão Brasil e não são cumulativos com pontos obtidos em outras divisões.<hr>
									6. Caso o membro não apresente boa conduta durante a sessão de exame ou de treinamento, o reporte poderá ser recusado.<hr>
									7. Válido para treinamentos e exames realizados a partir de 18/05/2018<hr>
								</div>
							</div>
						</div>
					</div>
					<div class="card-footer">
						<button onclick="formSend()" class="btn btn-success">Enviar Leg</button>
						<button onclick="closeForm()" class="btn btn-info">Fechar</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="container" class="container-fluid">
	<div class="row">
		<div id="pagina" class="col-lg-12">
			<div class="card">
				<div class="card-header">
					<i class="mr-2 fa fa-align-justify"></i>
					<strong class="card-title" v-if="headerText">Histórico de Legs</strong>
				</div>
				<div class="card-body">
					<table class="table table-borderless table-striped table-earning">
						<tbody>
							<tr>
									<th><strong>Reportadas: <?php echo $repor; ?></strong></th>
									<th><strong>Aprovadas: <?php echo $apprv; ?></strong></th>
									<th><strong>Rejeitadas: <?php echo $rejtd; ?></strong></th>
									<th><strong>Legs a serem modificadas: <?php echo $modif; ?></strong></th>
									<th colspan="2"><strong>Total de legs: <?php echo $total; ?></strong></th>
							</tr>
						</body>
					</table>
					<table class="table table-borderless table-striped table-earning">
						<thead>
							<tr>
								<th>ID</th>
								<th>Callsign</th>
								<th>Dia do voo</th>
								<th>Status</th>
								<th>Comentário</th>
								<th>Ações</th>
							</tr>
						</thead>
						<tbody>
							<?php $i = 0;foreach ($result as $legs) {
        $date = strtotime($legs['flight_day']);
        $date = date('d/m/Y', $date);?>
							<tr>
								<td><?php echo $legs['id']; ?></td>
								<td><?php echo $legs['callsign']; ?></td>
								<td><?php echo $date; ?></td>
								<td><?php echo "<span class=\"" . $class_status[$legs['status']] . "\">" . $status[$legs['status']] . "</span>"; ?></td>
								<td style="white-space: pre-wrap;"><?php echo $legs['val_rmk']; ?></td>
								<?php if ($legs['status'] == 2 || $legs['status'] == 3) {?>
								<td><button onclick="openModal(<?php echo $legs['id']; ?>)" class="btn btn-warning">Visualizar detalhes da leg</button></td>
								<?php } else {?>
								<td></td>
								<?php }?>
							</tr>
							<?php $i++;}?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>



<?php

    $i = 0;foreach ($result as $legs) {
        $date = $date = strtotime($legs['flight_day']);
        $date = date('d/m/Y', $date);
        ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div id="myModal<?php echo $legs['id']; ?>" style="display: none;" class="modal fade show" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="scrollmodalLabel">Modificar Leg</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span onclick="closeModal(<?php echo $legs['id']; ?>)">×</span>
							</button>
						</div>
						<div class="modal-body table-responsive">
							<table class="table table-borderless table-striped table-earning">
								<tr>
									<th>ID</th>
									<td><input type="text" id="idMod<?php echo $legs['id']; ?>" class="form-control" value="<?php echo $legs['id']; ?>" readonly="true"></td>
								</tr>
								<tr>
									<th>Callsign</th>
									<td><input type="text" id="callsignMod<?php echo $legs['id']; ?>" class="form-control" value="<?php echo $legs['callsign']; ?>"<?php if ($legs['status'] == 2) {echo 'readonly="true"';}?>></td>
								</tr>
								<tr>
									<th><b>Data *Preencher novamente</b></th>
									<td><input type="date" id="dateMod<?php echo $legs['id']; ?>" class="form-control" value=""<?php if ($legs['status'] == 2) {echo 'readonly="true"';}?>></td>
								</tr>
								<tr>
									<th>Comentário</th>
									<td><textarea type="text" id="rmkMod<?php echo $legs['id']; ?>" class="form-control" value="<?php echo $legs['rmk']; ?>" <?php if ($legs['status'] == 2) {echo 'readonly="true"';}?>></textarea></td>
								</tr>
								<tr>
									<th>Comentário do validador</th>
									<td><textarea type="text" id="val_rmkMod<?php echo $legs['id']; ?>" class="form-control" value="" readonly="true"><?php echo $legs['val_rmk']; ?></textarea></td>
								</tr>
							</table>
							<div class="form-actions center">
								<?php if ($legs['status'] == 3) {?>
								<button class="btn btn-success" onclick="modifyLeg('<?php echo $legs['id']; ?>')">Salvar modificação</button>
								<?php }?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $i++;}?>
<script>
function openModal(id)
{
	$
	(
		function ()
		{
			$("#myModal"+id).show();
		}
	);
}

function closeModal(id)
{
	$
	(
		function ()
		{
			$("#myModal"+id).hide();
		}
	);
}
</script>


<script type="text/javascript">

$( document ).ready(function()
{
	$("#legs").hide();
	$("#legPolicy").hide();
	$( "#form-reportar" ).hide();
}
);
function closeForm()
{
	$("#form-reportar").hide("#slow");
	$("#botao-reportar").show("slow");
}
function showLegPolicy()
{
	$("#legPolicy").show("slow");
}
function botaoReportarLeg()
{
	$("#botao-reportar").hide("slow");
	$("#form-reportar").show("#slow");
}
function formSend()
{
	$
	(
		function()
		{
			var data = {_function: "newLeg", id_session: $("#schedule").val(), vid: $("#vid").val(), callsign: $("#callsign").val(), rmk: $("#rmk").val(), date: $("#date").val()};
			$.post
			(
				"pages/functions/ps.php",
				{
					data: data
				},
				function(resultado)
				{
					if(resultado == "success")
					{
						alert("Leg registrada!");
						menuClick("td","points");
					}
					else if (resultado == "callP")
					{
						alert("Preencha o callsign");
					}
					else if (resultado == "callP")
					{
						alert("Selecione o exame/treinamento");
					}
					else if (resultado == "dateP")
					{
						alert("Preencha a data");
					}
					else
					{
						alert(resultado);
					}
				}
			);
		}
	);
}
function modifyLeg(id)
{
	console.log($("#dateMod"+id).val());
	$
	(
		function()
		{
			var data = {_function: "updateLeg", vid: 13,id_session: $("#idMod"+id).val(), id: id, callsign: $("#callsignMod"+id).val(), rmk: $("#rmkMod"+id).val(), date: $("#dateMod"+id).val()};
			$.post
			(
				"pages/functions/ps.php",
				{
					data: data
				},
				function(resultado)
				{
					if(resultado == "success")
					{
						alert("Leg modificada com sucesso!");
						menuClick("td","points");
					}else
					{
						alert(resultado);
					}
				}
			);
		}
	);
	}
</script>
<?#AQUI COMECA A PARTE EM INGLES ?>
<?php } else if ($_SESSION['lang'] == "EN") {
    $status = array("Reported", "Accepted", "Rejected", "Modification required");
    $class_status = array("label label-info", "label label-success", "label label-important", "label label-warning");
    ?>
<style type="text/css">.center{ text-align: center; }</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<div id="botao-reportar" class="container-fluid">
	<div class="row">
		<div id="pagina" class="col-lg-12">
			<div class="card">
				<div class="card-header">
					<i class="mr-2 fa fa-align-justify"></i>
					<strong class="card-title" v-if="headerText">Reporte one more leg</strong>
				</div>
				<div class="card-body">
					<a href="#" class="btn btn-outline-success" onclick="botaoReportarLeg()" id="botaoReportarLeg">Report Flight</a>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="form-reportar" class="container-fluid">
	<div class="row">
		<div id="pagina" class="col-lg-12">
			<div class="card">
				<div class="widget-box">
					<div class="card-header">
						<i class="mr-2 fa fa-align-justify"></i>
						<strong class="card-title" v-if="headerText">Reporte one more leg</strong>
					</div>
					<div class="card-body">
						<div class="form-horizontal">
							<div class="row form-group">
								<div class="col col-md-3">
									<label for="hf-email" class=" form-control-label">VID</label>
								</div>
								<div class="col-12 col-md-9">
									<input id="vid" type="text" class="form-control" readonly="true" value="<?php echo $member->vid; ?>" />
								</div>
							</div>
							<div class="row form-group">
								<div class="col col-md-3">
									<label for="hf-email" class=" form-control-label">Callsign</label>
								</div>
								<div class="col-12 col-md-9">
									<input id="callsign" type="text"  class="form-control" placeholder="Insert here your callsign" maxlength="7" />
								</div>
							</div>
							<div class="row form-group">
								<div class="col col-md-3">
									<label for="hf-email" class=" form-control-label">Remarks</label>
								</div>
								<div class="col-12 col-md-9">
									<textarea id="rmk" type="text"  class="form-control" placeholder="Insert here any remark you wish (Limited to 250 characters" maxlength="250" /></textarea>
								</div>
							</div>
							<div class="row form-group">
								<div class="col col-md-3">
									<label for="hf-email" class=" form-control-label">Flight Date</label>
								</div>
								<div class="col-12 col-md-9">
									<input id="date" type="date" class="form-control"/>
								</div>
							</div>
							<div class="row form-group">
								<div class="col col-md-3">
									<label for="hf-email" class=" form-control-label">Select the the exam/training</label>
								</div>
								<div class="col-12 col-md-9">
									<select id="schedule" class="form-control" required>
										<option selected disabled hidden>Select here the exam/training</option>
										<?php $i = 0;foreach ($schedule as $event) {
        $fiveDays = date("U", strtotime('-15 days'));
        if (($event['rating'] == "ADC" || $event['rating'] == "APC" || $event['rating'] == "ACC" || $event['rating'] == "SEC") && ($fiveDays < $event['data'])  && ($event['data'] <= date("U"))) {?>
										<option value="<?php echo $event['id']; ?>"><?php echo $event['rating'] . " " . $event['local'] . " " . date("d/m/Y", $event['data']) . " - Horário: " . $event['time'] . "Z"; ?></option>
										<?php }
        $i++;}?>
									</select>
								</div>
							</div>
							<div class="row form-group">
								<div class="col col-md-3">
									<label for="hf-email" class=" form-control-label">Policy</label>
								</div>
								<div class="col-12 col-md-9">
									<span class="help-block">By clicking on the <b>Request</b> button you agree with the following <b><a onclick="showLegPolicy()" href="#">policy</a></b></span><br>
								</div>
								<div class="col-12 col-md-9 controls" id="legPolicy">
									<hr>

									1. Are electable to obtain the Pilot Support Award all IVAO members that reach the minimum of:<br>
									&nbsp;&nbsp;&nbsp;&nbsp;a. Bronze - 10 points;<br>
									&nbsp;&nbsp;&nbsp;&nbsp;b. Silver - 20 points;<br>
									&nbsp;&nbsp;&nbsp;&nbsp;c. Gold - 40 points;<br>
									&nbsp;&nbsp;&nbsp;&nbsp;d. Platinum - 60 points;<br>
									&nbsp;&nbsp;&nbsp;&nbsp;e. Diamond - 80 points.<hr>

									2. The points will be obtained with your participation as a pilot during ATCs trainings and exams.<hr>

									3. Each participation (training or exam) will grant you 1 point, wherein the flight must accomplish with:<br>

									&nbsp;&nbsp;&nbsp;&nbsp;I. ADC Training/Exam:<br>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a. VFR - 30 minutes counted from departure time until landing time;<br>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b. IFR - 20 minutes minutes counted from departure time until landing time..<br>
									&nbsp;&nbsp;&nbsp;&nbsp;II. APC Training/Exam:<br>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a. VFR - 40 minutes counted from departure time until landing time;<br>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b. IFR - 45 minutes counted from departure time until landing time.<br>
									&nbsp;&nbsp;&nbsp;&nbsp;III. ACC Training/Exam:<br>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a. VFR - VFR - 45 minutes counted from departure time until landing time;<br>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b. IFR - 60 minutes counted from departure time until landing time.<br>
									&nbsp;&nbsp;&nbsp;&nbsp;IV. SEC Training/Exam:<br>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a. VFR - 40 minutes counted from departure time until landing time;<br>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b. IFR - minutes counted from departure time until landing time.<hr>
									4. Only one point is given for each training session.<hr>
									5. The points are exclusely from the Brazilian division and the ARE NOT cumulative with points received from anothers divisions.<hr>
									6. If the member don't behave well during the training session, the report may be rejected.<hr>
									7. Only valid for training and exams performed after 18/05/2018.<hr>
								</div>
							</div>
						</div>
					</div>
					<div class="card-footer">
						<button onclick="formSend()" class="btn btn-success">Send Leg</button>
						<button onclick="closeForm()" class="btn btn-info">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="container" class="container-fluid">
	<div class="row">
		<div id="pagina" class="col-lg-12">
			<div class="card">
				<div class="card-header">
					<i class="mr-2 fa fa-align-justify"></i>
					<strong class="card-title" v-if="headerText">Legs History</strong>
				</div>
				<div class="card-body table-responsive">
					<table class="table table-borderless table-striped table-earning">
						<tbody>
							<tr>
								<th><strong>Reported legs: <?php echo $repor; ?></strong></th>
								<th><strong>Approved legs: <?php echo $apprv; ?></strong></th>
								<th><strong>Rejected legs: <?php echo $rejtd; ?></strong></th>
								<th><strong>To be modified: <?php echo $modif; ?></strong></th>
								<th colspan="2"><strong>Total: <?php echo $total; ?></strong></th>
							</tr>
						</body>
					</table>
					<table class="table table-borderless table-striped table-earning">
						<thead>
							<tr>
								<th>ID</th>
								<th>Callsign</th>
								<th>Flight Date</th>
								<th>Status</th>
								<th>Remarks</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php $i = 0;foreach ($result as $legs) {
        $date = strtotime($legs['flight_day']);
        $date = date('d/m/Y', $date);?>
							<tr>
								<td><?php echo $legs['id']; ?></td>
								<td><?php echo $legs['callsign']; ?></td>
								<td><?php echo $date; ?></td>
								<td><?php echo "<span class=\"" . $class_status[$legs['status']] . "\">" . $status[$legs['status']] . "</span>"; ?></td>
								<td style="white-space: pre-wrap;"><?php echo $legs['val_rmk']; ?></td>
								<?php if ($legs['status'] == 2 || $legs['status'] == 3) {?>
								<td><button onclick="openModal(<?php echo $legs['id']; ?>)" class="btn btn-warning">Show Details</button></td>
								<?php } else {?>
								<td></td>
								<?php }?>
							</tr>
							<?php $i++;}?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>



<?php

    $i = 0;foreach ($result as $legs) {
        $date = $date = strtotime($legs['flight_day']);
        $date = date('d/m/Y', $date);
        ?>
	<div id="myModal<?php echo $legs['id']; ?>" style="display: none;" class="modal fade show" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="scrollmodalLabel">Modify Leg</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span onclick="closeModal(<?php echo $legs['id']; ?>)">×</span>
					</button>
				</div>
				<div class="modal-body">
					<table class="table table-borderless table-striped table-earning">
						<tr>
							<th>ID</th>
							<td><input type="text" id="idMod<?php echo $legs['id']; ?>" class="form-control" value="<?php echo $legs['id']; ?>" readonly="true"></td>
						</tr>
						<tr>
							<th>Callsign</th>
							<td><input type="text" id="callsignMod<?php echo $legs['id']; ?>" class="form-control" value="<?php echo $legs['callsign']; ?>"<?php if ($legs['status'] == 2) {echo 'readonly="true"';}?>></td>
						</tr>
						<tr>
							<th><b>Date *Fill out again</b></th>
							<td><input type="date" id="dateMod<?php echo $legs['id']; ?>" class="form-control" value=""<?php if ($legs['status'] == 2) {echo 'readonly="true"';}?>></td>
						</tr>
						<tr>
							<th>Remark</th>
							<td><textarea type="text" id="rmkMod<?php echo $legs['id']; ?>" class="form-control" value="<?php echo $legs['rmk']; ?>" <?php if ($legs['status'] == 2) {echo 'readonly="true"';}?>></textarea></td>
						</tr>
						<tr>
							<th>Validator Remark</th>
							<td><textarea type="text" id="val_rmkMod<?php echo $legs['id']; ?>" class="form-control" value="" readonly="true"><?php echo $legs['val_rmk']; ?></textarea></td>
						</tr>
					</table>
					<div class="form-actions center">
						<?php if ($legs['status'] == 3) {?>
						<button class="btn btn-success" onclick="modifyLeg('<?php echo $legs['id']; ?>')">Save</button>
						<?php }?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php $i++;}?>
<script>
function openModal(id)
{
	$
	(
		function ()
		{
			$("#myModal"+id).show();
		}
	);
}

function closeModal(id)
{
	$
	(
		function ()
		{
			$("#myModal"+id).hide();
		}
	);
}
</script>


<script type="text/javascript">

$( document ).ready(function()
{
	$("#legs").hide();
	$("#legPolicy").hide();
	$( "#form-reportar" ).hide();
}
);
function closeForm()
{
	$("#form-reportar").hide("#slow");
	$("#botao-reportar").show("slow");
}
function showLegPolicy()
{
	$("#legPolicy").show("slow");
}
function botaoReportarLeg()
{
	$("#botao-reportar").hide("slow");
	$("#form-reportar").show("#slow");
}
function formSend()
{
	$
	(
		function()
		{
			var data = {_function: "newLeg", id_session: $("#schedule").val(), vid: $("#vid").val(), callsign: $("#callsign").val(), rmk: $("#rmk").val(), date: $("#date").val()};
			$.post
			(
				"pages/functions/ps.php",
				{
					data: data
				},
				function(resultado)
				{
					if(resultado == "success")
					{
						alert("Leg registered!");
						menuClick("td","points");
					}
					else if (resultado == "callP")
					{
						alert("Fill out callsign");
					}
					else if (resultado == "sessP")
					{
						alert("Select the training/exam");
					}
					else if (resultado == "dateP")
					{
						alert("Fill out the date");
					}
					else
					{
						alert(resultado);
					}
				}
			);
		}
	);
}
function modifyLeg(id)
{
	$
	(
		function()
		{
			var data = {_function: "updateLeg", vid: 13,id_session: $("#idMod").val(), id: id, callsign: $("#callsignMod"+id).val(), rmk: $("#rmkMod"+id).val(), date: $("#dateMod"+id).val()};
			$.post
			(
				"pages/functions/ps.php",
				{
					data: data
				},
				function(resultado)
				{
					if(resultado == "success")
					{
						alert("Leg successfully modified!");
						$("#myModal"+id).hide();
						menuClick("td","points");
					}else
					{
						alert(resultado);
					}
				}
			);
		}
	);
	}
</script>
<?php }?>
<style>
td
{
	text-align: center;
}
</style>
