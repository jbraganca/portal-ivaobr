<?php 

if(isset($_GET['function']))
	$function = $_GET['function'];
else 
	$function = $_POST['functionExecute'];

if($function == "timetable")
{
	date_default_timezone_set('UTC'); 
	include("../../classes.php");
	$division = new division();
	$flights = $division->getFlights();	
	$wzp = "/home/brhq01/public_html/whazzup/zapzap.txt";
	$ponteiro = fopen($wzp,"r");
	$i=0;
	$airac = carregaAirac();
	while(!feof($ponteiro))
	{
		$dado = fgets($ponteiro);
		if(strstr($dado,":"))
		{
			$dadoArray = explode(":",$dado);
			foreach($flights as $flight)
			{
				if($dadoArray[0] == $flight['call'])
				{
					//print_r($dadoArray);
					$return[$i]['call'] = $dadoArray[0];
					$return[$i]['eobt'] = str_pad($dadoArray[23],4,"0",STR_PAD_LEFT);
					$return[$i]['depAd'] = $dadoArray[11];
					$return[$i]['destAd'] = $dadoArray[13];
					$return[$i]['acft'] = substr($dadoArray[9],2,4);
					$return[$i]['altitude'] = round($dadoArray[7],-3);
					$return[$i]['cruiseFl'] = $dadoArray[12];
					$return[$i]['lat'] = $dadoArray[5];
					$return[$i]['lon'] = $dadoArray[6];
					$return[$i]['gs'] = $dadoArray[8];
					
					$acftPoint['lat'] = $return[$i]['lat'];
					$acftPoint['lon'] = $return[$i]['lon'];
					
					$return[$i]['depDist'] = distanceCalculation($acftPoint, $return[$i]['depAd'],$airac);
					$return[$i]['destDist'] = distanceCalculation($acftPoint, $return[$i]['destAd'],$airac);
					
					if($return[$i]['destDist'] > 0)					
						$return[$i]['eta'] = date("H:i",round(date("U")+($return[$i]['destDist']/$return[$i]['gs'])*3600,0));
					else
						$return[$i]['eta'] = "N/A";
					
					$return[$i]['now'] = date("U e G:i");
					
					
					if(strstr($return[$i]['cruiseFl'],"F"))
					{
						$return[$i]['cruiseFl'] = str_replace("F","",$return[$i]['cruiseFl'])."00";
					}
					else if(strstr($return[$i]['cruiseFl'],"A"))
					{
						$return[$i]['cruiseFl'] = str_replace("A","",$return[$i]['cruiseFl'])."00";
					}
					
					
					if($dadoArray[46] == '1' && $return[$i]['depDist'] < 5)
					{
						$return[$i]['status'] = "Boarding";
					}
					else if($dadoArray[46] == '1' && $return[$i]['destDist'] < 5)
					{
						$return[$i]['status'] = "Landed";
					}
					else if($return[$i]['cruiseFl'] == $return[$i]['altitude'])
					{
						$return[$i]['status'] = "Cruise";
					}
					else if($return[$i]['cruiseFl'] > $return[$i]['altitude'] && ($return[$i]['depDist'] < 100 || $return[$i]['destDist'] < 100))
					{
						if($return[$i]['destDist'] < $return[$i]['depDist'])
						{
							$return[$i]['status'] = "Arriving";
						}
						else
						{
							$return[$i]['status'] = "Departing";
						}
					}
					else
						$return[$i]['status'] = "Cruise";
					$i++;
				}
			}
		}
		
	}
	if(isset($_GET['function']))
	{
		echo "<pre>";
		print_r($return);
		echo "</pre>";
	}
	else
	echo json_encode($return);
	
}
function carregaAirac()
{
	$airac = "/home/brhq01/public_html/sectorfile/source/navdata/AIRPORTS.dat";
	$ponteiro = fopen($airac,"r");
	$i=0;
	while(!feof($ponteiro))
	{
		$linha = fgets($ponteiro);
		if(!strstr($linha,";"))
		{
			$return[$i] = $linha;
			$i++;
		}
	}
	return $return;
}
function distanceCalculation($acft, $icao, $airac)
{
	//NZWB-41.518333 173.870278
	$achou = false;
	foreach($airac as $line)
	{
		$icaoData = substr($line,0,4);
		$lat = substr(str_replace($icao,"",$line),0,10);
		$lon = str_replace($lat,"",str_replace($icao,"",$line));
		
		if($icaoData == trim($icao))
		{
			$achou = true;
			break;
		}
	}
	if($achou)
	{
		$dlat = ($acft['lat'] - $lat)*60;
		if($dlat < 0) $dlat = -$dlat;
		
		$dlon = ($acft['lon'] - $lon)*60;
		if($dlon < 0) $dlon = - $dlon;
		
		$dist = sqrt(pow($dlat,2)+pow($dlon,2));
		
		return $dist;
	}
	else
	{
		return -1;
	}
}

?>