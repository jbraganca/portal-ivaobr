<?php

$member = new member();
if (!$member->isConnected()) {
    $member->loginValidate();
} else {
    $member->getData();
}
if ($_SESSION['lang'] == "PT") {
    ?>
  <div class="container-fluid">
    <div class="row-fluid">
      <div class="col-sm-12">
        <div class="alert alert-warning" role="alert" style="text-align: center;">
          Atenção, para saber de todas as regras sobre a transferência de rating, por favor, dirija-se
          <a href="https://www.ivao.aero/training/ratings/rating_transfer.asp" class="alert-link" target="_blank">a esta página da IVAO</a>
        </div>
      </div>
    </div>
    <div class="row-fluid">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h4>Escolha qual modalidade de transferência deseja realizar</h4>
          </div>
          <div class="card-body">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
    					<li class="nav-item">
    						<a class="nav-link active show" id="history-tab" data-toggle="tab" href="#history" role="tab" aria-controls="history" aria-selected="true">Histórico</a>
    					</li>
    					<li class="nav-item">
    						<a class="nav-link" id="atc-tab" data-toggle="tab" href="#atc" role="tab" aria-controls="atc" aria-selected="false">Controlador (VATSIM)</a>
    					</li>
    					<li class="nav-item">
    						<a class="nav-link" id="piloto-tab" data-toggle="tab" href="#piloto" role="tab" aria-controls="piloto" aria-selected="false">Piloto (ANAC)</a>
    					</li>
    				</ul>
            <div class="tab-content pl-3 p-1" id="categoriaXfer">
    					<div class="tab-pane fade active show" id="history" role="tabpanel" aria-labelledby="history-tab">
    						<br><h3>Histórico de solicitações</h3><br>
    						<div class="row">
                  <div class="col-xs-12">
                      <div class="table-responsive table--no-card m-b-30">
                          <table class="table table-borderless table-striped table-earning">
                              <thead>
                                  <tr>
                                      <th># Protocolo</th>
                                      <th>Modalidade</th>
                                      <th>Rating</th>
                                      <th>Status</th>
                                      <th>Solicitado em</th>
                                      <th>Finalizado em</th>
                                      <th>Comentarios</th>
                                      <th>Detalhes</th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <tr>
                                      <td>2018082425REX4</td>
                                      <td>ATC</td>
                                      <td>ADC</td>
                                      <td>Validado BR-TD</td>
                                      <td>2018-08-04</td>
                                      <td>-</td>
                                      <td>-------</td>
                                      <td>Mais detalhes</td>
                                  </tr>
                              </tbody>
                          </table>
                      </div>
                  </div>
                </div>
    					</div>
    					<div class="tab-pane fade" id="atc" role="tabpanel" aria-labelledby="atc-tab">
    						<h3>Transferência VATSIM</h3>
    						<p>Some content here.</p>
    					</div>
    					<div class="tab-pane fade" id="piloto" role="tabpanel" aria-labelledby="piloto-tab">
    						<h3>Transferência cANAC</h3>
    						<p>Some content here.</p>
    					</div>
    				</div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="../portal/pages/functions/ratingXfer.js"></script>
<?php
}
?>
