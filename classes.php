<?php
class page
{
    public $url;

    private function getURL()
    {
        $dominio= $_SERVER['HTTP_HOST'];
        $url = "http://" . $dominio. $_SERVER['REQUEST_URI'];
        $url = str_replace("http://ivao.com.br/portal/", "", $url);
        $url = str_replace("pages/", "", $url);
        $url = str_replace(".php", "", $url);
        if ($url == "") {
            $url = "Index";
        }
        $this->url = $url;
    }

    public function getBreadcumb()
    {
        $this->getURL();
        $data = '<ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Portal</a>
          </li>
          <li class="breadcrumb-item active">'.$this->url;
        $data.= '</li></ol>';
        echo $data;
    }
}
class database
{
    public $server;
    public $user;
    public $pass;
    public $db;
    private $conn;
    public $query;
    public $fetch_result;
    public $num_rows;

    public function connect()
    {
        $host = $this->server;
        $db = $this->db;
        $senha_db = $this->pass;
        $user_db = $this->user;

        if ($host == "" || $db == ""  || $user_db == "" || $senha_db == "") {
            return false;
        } else {
            $this->conn = new PDO("mysql:host=$host;dbname=$db", $user_db, $senha_db, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $this->conn->exec("set names utf8");
            return true;
        }
    }

    public function search()
    {
        if ($this->query == "") {
            echo "Query Empty";
            return false;
        } else {
            $q = $this->conn->prepare($this->query);
            $q->execute();
            $this->fetch_result = $q->fetchAll(PDO::FETCH_ASSOC);
            $this->num_rows = $q->rowCount();
            return true;
        }
    }
}

class division
{
    public $db;

    public function getFlights()
    {
        /*require_once "/home/brhq01/public_html/includes/config/config.php";

        $this->db = new database();
        $this->db->server = DBASE_SERVER;
        $this->db->user = "root";
        $this->db->pass = DBASE_PASS;
        $this->db->db = "ssr";

        if(!$this->db->connect())
        {
            echo "DB Error!";
            exit;
        }

        $this->db->query = "SELECT*FROM realtime";
        $this->db->search();
        return $this->db->fetch_result;*/
        $wzp = "/home/brhq01/public_html/whazzup/zapzap.txt";
        $ponteiro = fopen($wzp, "r");
        $i=0;
        while (!feof($ponteiro)) {
            $linha = fgets($ponteiro);
            if (strstr($linha, ":")) {
                $dado = explode(":", $linha);
                $dep = $dado[11];
                $dest = $dado[12];
                $call = $dado[0];
                if ($dep[0] == "S" && ($dep[1] == "B" || $dep[1] == "D" || $dep[1] == "I" || $dep[1] == "J" || $dep[1] == "N" || $dep[1] == "S") || $dep[1] == "W") {
                    $return[$i]['call'] = $call;
                    $i++;
                }
                if ($dest[0] == "S" && ($dest[1] == "B" || $dest[1] == "D" || $dest[1] == "I" || $dest[1] == "J" || $dest[1] == "N" || $dest[1] == "S") || $dest[1] == "W") {
                    $return[$i]['call'] = $call;
                    $i++;
                }
            }
        }
        return $return;
    }

    public function getNOTAMs()
    {
        $db = $this->db;
        $db->query = "SELECT*FROM cms_notam ORDER by id DESC";
        $db->search();
        $return = $db->fetch_result;
        return $return;
    }

    public function getTrainingsStats()
    {
        $db = $this->db;
        $vid = $this->vid;
        /* TREINAMENTOS DE RATING REALIZADOS */
        $db->query = "SELECT * FROM `t_trainings` WHERE status = '2' AND result !='2' AND trainingType != 'Inicial Piloto' AND trainingType != 'Inicial ATC'";
        $db->search();
        $return[0] = $db->num_rows;

        /* TREINAMENTOS INICIAIS REALIZADOS */
        $db->query = "SELECT * FROM `t_trainings` WHERE status = '2' AND result !='2' AND (trainingType = 'Inicial Piloto' OR trainingType = 'Inicial ATC')";
        $db->search();
        $return[1] = $db->num_rows;

        /* TREINMANEOTS ATC REALIZADOS */
        $db->query = "SELECT * FROM `t_trainings` WHERE status = '2' AND result !='2' AND (trainingType = 'ADC' OR trainingType = 'APC' OR trainingType = 'ACC' OR trainingType = 'Inicial ATC')";
        $db->search();
        $return[2] = $db->num_rows;

        /* TREINMANEOTS PILOTO REALIZADOS */
        $db->query = "SELECT * FROM `t_trainings` WHERE status = '2' AND result !='2' AND (trainingType = 'PP' OR trainingType = 'SPP' OR trainingType = 'CP' OR trainingType = 'Inicial Piloto')";
        $db->search();
        $return[3] = $db->num_rows;

        return $return;
    }
    public function getEventsStats()
    {
        $db = $this->db;
        $vid = $this->vid;
        /* TREINAMENTOS DE RATING REALIZADOS */
        $db->query = "SELECT * FROM `t_trainings` WHERE status = '2' AND result !='2' AND trainingType != 'Inicial Piloto' AND trainingType != 'Inicial ATC'";
        $db->search();
        $return[0] = $db->num_rows;

        /* TREINAMENTOS INICIAIS REALIZADOS */
        $db->query = "SELECT * FROM `t_trainings` WHERE status = '2' AND result !='2' AND (trainingType = 'Inicial Piloto' OR trainingType = 'Inicial ATC')";
        $db->search();
        $return[1] = $db->num_rows;

        /* TREINMANEOTS ATC REALIZADOS */
        $db->query = "SELECT * FROM `t_trainings` WHERE status = '2' AND result !='2' AND (trainingType = 'ADC' OR trainingType = 'APC' OR trainingType = 'ACC' OR trainingType = 'Inicial ATC')";
        $db->search();
        $return[2] = $db->num_rows;

        /* TREINMANEOTS PILOTO REALIZADOS */
        $db->query = "SELECT * FROM `t_trainings` WHERE status = '2' AND result !='2' AND (trainingType = 'PP' OR trainingType = 'SPP' OR trainingType = 'CP' OR trainingType = 'Inicial Piloto')";
        $db->search();
        $return[3] = $db->num_rows;

        return $return;
    }

    public function getTDBooks()
    {
        $db = $this->db;
        $db->query = "SELECT*FROM t_schedule ORDER BY data ASC";
        $db->search();
        $return = $db->fetch_result;
        return $return;
    }
}
class member
{
    public $vid;
    public $firstname;
    public $lastname;
    public $rating;
    public $ratingatc;
    public $ratingpilot;
    public $division;
    public $country;
    public $skype;
    public $hoursatc;
    public $hourspilot;
    public $isNPO;
    public $vastaffid;
    public $vastaff;
    public $vastafficaos;
    public $staff;

    public $db;
    public $pointEventQtde;
    public $pointEventDesc;

    public function getEventPoints()
    {
        $db = $this->db;
        $vid = $this->vid;
        $db->query = "SELECT*FROM t_pontos_eventos WHERE vid = '$vid'";
        $db->search();
        $this->pointEventQtde = $db->num_rows;
        $this->pointEventDesc = $db->fetch_result;
    }

    public function getEventData()
    {
        $db = $this->db;
        $vid = $this->vid;
        $db->db = "brhq01_trackingbr";
        $db->connect();
        $db->query = "SELECT*FROM t_tracking_data WHERE vid = '$vid'";
        $db->search();
        $resultado = $db->fetch_result;

        $rows = sizeof($resultado);

        for ($i = 0; $i < $rows; $i++) {
            $id_trk = $resultado[$i]['idTrk'];
            $db->query = "SELECT*FROM t_tracking WHERE id = '$id_trk'";
            $db->Search();
            $resultado1 = $db->fetch_result;
            $resultado[$i]['event'] = $resultado1[0]['dsEvento'];
            $resultado[$i]['dataStart'] = strtotime($resultado1[0]['dataStart']);
            $resultado[$i]['dataEnd'] = strtotime($resultado1[0]['dataEnd']);
            if ($resultado[$i]['totalTime'] == "") {
                unset($resultado[$i]);
            }
        }
        return $resultado;
    }

    public function getTrainings()
    {
        $db = $this->db;
        $vid = $this->vid;
        // FITLRA SÓ OS SEUS TREINAMENTOS
        $db->query = "SELECT*FROM t_trainings WHERE vid = '$vid'";
        // MOSTRA TODOS TREINAMENTOS
        //$db->query = "SELECT*FROM t_trainings";
        //$db->query = "SELECT * FROM `t_trainings` WHERE Status != 'Finalizado' AND Status != 'Expirado'";
        $db->search();
        return $db->fetch_result;
    }

    public function registerTraining($data)
    {
        $vid = $this->vid;
        $name = $this->firstname." ".$this->lastname;
        $training = $data['training'];

        if ($training == "ATC Initial" || $training == "ATC Inicial") {
            $training = "Inicial ATC";
        } elseif ($training == "Piloto Inicial" || $training == "Pilot Initial") {
            $training = "Inicial Piloto";
        }

        $email = $data['email'];
        $disp = $data['date1']." ".$data['option1']."<br>".$data['date2']." ".$data['option2']."<br>".$data['date3']." ".$data['option3']."<br>";
        $db = $this->db;
        $db->query = "SELECT*FROM t_trainings WHERE vid = '$vid' AND trainingType = '$training' AND status < 2";
        $db->search();
        $today = new DateTime();
        $today = $today->getTimeStamp();
        $rows = $db->num_rows;

        if ($rows < 1) {
            $db->query = "SELECT*FROM t_trainings WHERE vid = '$vid' AND trainingType = '$training' AND status >= 2 ORDER BY closeData DESC";
            $db->search();
            $dif = round(($today-$db->fetch_result[0]['closeData'])/24/3600, 0, PHP_ROUND_HALF_DOWN);
            $falta = 15-$dif;

            if ($dif < 15) {
                if ($_SESSION['lang'] == "PT") {
                    echo "Você solicitou o treinamento $training nos últimos 15 dias. Um novo treinamento estará disponível em $falta dias";
                } elseif ($_SESSION['lang'] == "EN") {
                    echo "You already requested $training training in the last 15 days. A new training will be available in $falta days";
                }
                return false;
            }

            $q = "INSERT INTO t_trainings (name, vid, email, trainingType, memberAvailability,status,requestData,trainingPolicy) VALUES ('$name', '$vid', '$email', '$training', '$disp','0','$today','0')";
            $db->query = $q;
            $db->search();
            echo "success";
            return true;
        } else {
            echo "TrainingRepetido";
            return false;
        }
    }

    public function getLegsPS()
    {
        $db = $this->db;
        $vid = $this->vid;
        // FILTRA SOMENTE AS LEGS PESSOAIS
        $db->query = "SELECT * FROM t_points WHERE vid = '$vid' ORDER BY id DESC ";
        $db->search();
        return $db->fetch_result;
    }

    public function registerLegPS($data)
    {
        $vid = $this->vid;
        $status = "0";
        $aw_bronze = "0";
        $aw_silver = "0";
        $aw_gold = "0";
        $aw_platinum = "0";
        $aw_diamond = "0";
        $training_id = $data['id_session'];
        $flight_day = strtotime($data['date']);
        $flight_day = date('Y-m-d', $flight_day);
        $callsign = strtoupper($data['callsign']);
        $rmk = $data['rmk'];
        $db = $this->db;
        $db->query = "SELECT * FROM t_points WHERE vid = '$vid' AND id_session = '$training_id' ";
        $db->search();
        $rows = $db->num_rows;
        if ($rows == 0) {
            $db->query = "SELECT * FROM t_points WHERE vid = '$vid' AND aw_bronze = 1 ";
            $db->search();
            $rows = $db->num_rows;
            if ($rows >= 1) {
                $aw_bronze = 1;
            }
            $db->query = "SELECT * FROM t_points WHERE vid = '$vid' AND aw_silver = 1 ";
            $db->search();
            $rows = $db->num_rows;
            if ($rows >= 1) {
                $aw_silver = 1;
            }
            $db->query = "SELECT * FROM t_points WHERE vid = '$vid' AND aw_gold = 1 ";
            $db->search();
            $rows = $db->num_rows;
            if ($rows >= 1) {
                $aw_gold = 1;
            }
            $db->query = "SELECT * FROM t_points WHERE vid = '$vid' AND aw_platinum = 1 ";
            $db->search();
            $rows = $db->num_rows;
            if ($rows >= 1) {
                $aw_platinum = 1;
            }
            $db->query = "SELECT * FROM t_points WHERE vid = '$vid' AND aw_diamond = 1 ";
            $db->search();
            $rows = $db->num_rows;
            if ($rows >= 1) {
                $aw_diamond = 1;
            }
            $q = "INSERT INTO t_points (id_session, vid, callsign, flight_day, status, rmk, aw_bronze, aw_silver, aw_gold, aw_platinum, aw_diamond, last_updatedBy) VALUES ('$training_id', '$vid', '$callsign', '$flight_day', '$status', '$rmk', '$aw_bronze', '$aw_silver', '$aw_gold', '$aw_platinum', '$aw_diamond', '$vid')";
            $db->query = $q;
            $db->search();
            echo "success";
            return true;
        } else {
            echo "Voce ja reportou uma leg para sessão/You have already reported one leg related to this session";
            return true;
        }
    }

    public function updateLegPS($data)
    {
        $vid = $this->vid;
        $id = $data['id'];
        $flight_day = strtotime($data['date']);
        $flight_day = date('Y-m-d', $flight_day);
        $callsign = strtoupper($data['callsign']);
        $rmk = $data['rmk'];
        $db = $this->db;
        $q = "UPDATE t_points SET callsign = '$callsign', flight_day = '$flight_day', status = '0', rmk = '$rmk', last_updatedBy = '$vid' WHERE id = '$id'";
        $db->query = $q;
        $db->search();
        echo "success";
        return true;
    }

    public function printEvent()
    {
        print_r($this->pointEventDesc);
    }

    public function isConnected()
    {
        session_start();
        if (isset($_SESSION['lang']) && isset($_SESSION['vid'])) 
		{
            return true;
        } 
		else 
		{
            return false;
        }
    }
    public function loginValidate()
    {
        session_start();
		$URL_ATUAL = "http://$_SERVER[HTTP_HOST]";
        header("Location:http://login.ivao.aero/index.php?url=$URL_ATUAL/portal/loginvalidation.php");
    }

    private function actualURL()
    {
        $dominio= $_SERVER['HTTP_HOST'];
        $url = "http://" . $dominio. $_SERVER['REQUEST_URI'];
        return $url;
    }


    public function logout()
    {
        session_destroy();
        $url = $this->actualURL();
        header("Location:$url");
    }

    public function getData()
    {
        session_start();
        $this->vid = $_SESSION['vid'];
        $this->firstname = $_SESSION['firstname'];
        $this->lastname = $_SESSION['lastname'];
        $this->ratingatc = $_SESSION['ratingatc'];
        $this->ratingpilot = $_SESSION['ratingpilot'];
        $this->division = $_SESSION['division'];
        $this->skype = $_SESSION['skype'];
        $this->hoursatc = $_SESSION['hours_atc'];
        $this->hourspilot = $_SESSION['hours_pilot'];
        $this->isNPO = $_SESSION['isNpoMember'];
        $this->vastaffid = $_SESSION['va_staff_ids'];
        $this->vastaff = $_SESSION['va_staff'];
        $this->vastafficaos = $_SESSION['va_staff_icaos'];
        $this->staff = $_SESSION['staff'];




		$dir = $_SERVER['DOCUMENT_ROOT'];

		require_once "$dir/includes/functions/functions.php";
		require_once "$dir/includes/config/config.php";

        $this->db = new database();
        $this->db->server = DBASE_SERVER;
        $this->db->user = DBASE_USER;
        $this->db->pass = DBASE_PASS;
        $this->db->db = DBASE_NAME;
        if (!$this->db->connect()) {
            echo "DB Error!";
            exit;
        }
    }
    public function getLegs($data)
    {
        $vid = $this->vid;
        $crit = $data;
        $db = $this->db;
        $q = "SELECT vid, count(vid) as total FROM t_points WHERE vid = '$vid' AND status='$crit'";
        $db->query = $q;
        $db->search();
        return $db->fetch_result;
    }

    public function requestGCA($data)
    {
        $vid = $this->vid;
        $div = $this->division;
        $rating = $this->ratingatc;
        $email = $data['email'];
        $name = $this->firstname." ".$this->lastname;
        $rmk = $data['rmk'];
        $disp = $data['date1']." ".$data['option1']."<br>".$data['date2']." ".$data['option2']."<br>".$data['date3']." ".$data['option3']."<br>";
        $db = $this->db;
        $db->query = "SELECT*FROM t_gca WHERE vid = '$vid' AND status < 3";
        $db->search();
        $today = new DateTime();
        $today = $today->getTimeStamp();
        $rows = $db->num_rows;

        if ($rows < 1) {
            $db->query = "SELECT*FROM t_gca WHERE vid = '$vid' ORDER BY close_data DESC";
            $db->search();
            $dif = round(($today-$db->fetch_result[0]['close_data'])/24/3600, 0, PHP_ROUND_HALF_DOWN);
            $falta = 30-$dif;

            if ($dif <= 30) {
                if ($_SESSION['lang'] == "EN") {
                    echo "You already have requested a GCA in the last 30 days. You may reqyest it again in $falta days";
                }
                return false;
            }

            $q = "INSERT INTO t_gca (name, curr_div, req_date, dates, status, rating, email, member_rmk, vid) VALUES ('$name', '$div', '$today', '$disp', '0', '$rating', '$email', '$rmk', '$vid')";
            $db->query = $q;
            $db->search();
            echo "success";
            return true;
        }
    }
}
