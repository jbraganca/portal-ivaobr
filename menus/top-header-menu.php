<?php 

if($_SESSION['lang'] == "PT")
{

?>

<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">
    <li  class="dropdown" id="profile-messages" ><a title="" href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle"><i class="icon icon-user"></i>  <span class="text">Bem-vindo <?php echo $member->firstname;?></span><b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li><a href="#"><i class="icon-user"></i>Meu Perfil</a></li>
        <li class="divider"></li>
        <li><a href="logout.php"><i class="icon-key"></i>Sair</a></li>
      </ul>
    </li>
    <li  class="dropdown" id="language"><a title="" href="#" data-toggle="dropdown" data-target="#language" class="dropdown-toggle"><i class="icon icon-flag"></i>  <span class="text">Idioma</a>
      <ul class="dropdown-menu">
        <li><a href="language.php?lang=EN"><i></i>Inglês</a></li>
        <li class="divider"></li>
        <li><a href="language.php?lang=PT"><i></i>Português</a></li>
      </ul>
    </li>
    <li class=""><a title="" href="logout.php"><i class="icon icon-share-alt"></i> <span class="text">Sair</span></a></li>
  </ul>
</div>

<?php
}
else if($_SESSION['lang'] == "EN")
{
?>

<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">
    <li  class="dropdown" id="profile-messages" ><a title="" href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle"><i class="icon icon-user"></i>  <span class="text">Welcome <?php echo $member->firstname;?></span><b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li><a href="#"><i class="icon-user"></i>My Profile</a></li>
        <li class="divider"></li>
        <li><a href="logout.php"><i class="icon-key"></i>Log Out</a></li>
      </ul>
    </li>
    <li  class="dropdown" id="language"><a title="" href="#" data-toggle="dropdown" data-target="#language" class="dropdown-toggle"><i class="icon icon-flag"></i>  <span class="text">Language</a>
      <ul class="dropdown-menu">
        <li><a href="language.php?lang=EN"><i></i>English</a></li>
        <li class="divider"></li>
        <li><a href="language.php?lang=PT"><i></i>Portuguese</a></li>
      </ul>
    </li>
    <li class=""><a title="" href="logout.php"><i class="icon icon-share-alt"></i> <span class="text">Logout</span></a></li>
  </ul>
</div>

<?php 

}
?>