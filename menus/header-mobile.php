<?php if ($_SESSION['lang'] == "PT") {
    ?>
<header class="header-mobile d-block d-lg-none">
	<div class="header-mobile__bar">
		<div class="container-fluid">
			<div class="header-mobile-inner">
				<a class="logo" href="index.html">
					<img src="images/icon/logo.png" alt="CoolAdmin" />
				</a>
				<button class="hamburger hamburger--slider" type="button">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</button>
			</div>
		</div>
	</div>
	<nav class="navbar-mobile">
		<div class="container-fluid">
			<ul class="navbar-mobile__list list-unstyled">
				<li class="active has-sub">
					<a onclick="menuClick('index','home')" class="js-arrow" href="#"><i class="fas fa-home"></i>Início</a>
				</li>
				<li class="has-sub" onclick="changeArrow()">
					<a class="js-arrow" href="#">
						<i class="fas fa-file"></i>Treinamento&nbsp;
						<i id="arrow" class="fa fa-chevron-left j-dropdown"></i>
					</a>
					<ul class="list-unstyled navbar__sub-list js-sub-list">
						<li><a href="#" onclick="menuClick('td','home')">Geral</a></li>
						<li><a href="#" onclick="menuClick('td','mytrainings')">Meus Treinamentos</a></li>
						<li><a href="#" onclick="menuClick('td','newtraining')">Solicitar Treinamento</a></li>
						<li><a href="#" onclick="menuClick('td','schedule')">Agenda TD</a></li>
						<li><a href="#" onclick="menuClick('td','points')">Reporte - Pilot Support</a></li>
						<?php /*<li><a href="#" onclick="menuclick('td','gcacheckout')">GCA</a></li> */ ?>
						<?php if ($member->vid == "379042" || $member->vid == "316499") {
        echo '<li><a href="#" onclick="menuClick(\'td\',\'ratingXfer\')">Transferir rating</a></li>';
    } ?>
					</ul>
				</li>
			</ul>
		</div>
	</nav>
</header>
<?php
} elseif ($_SESSION['lang'] == "EN") {
        ?>
<header class="header-mobile d-block d-lg-none">
	<div class="header-mobile__bar">
		<div class="container-fluid">
			<div class="header-mobile-inner">
				<a class="logo" href="index.html">
					<img src="images/icon/logo.png" alt="CoolAdmin" />
				</a>
				<button class="hamburger hamburger--slider" type="button">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</button>
			</div>
		</div>
	</div>
	<nav class="navbar-mobile">
		<div class="container-fluid">
			<ul class="navbar-mobile__list list-unstyled">
				<li class="active has-sub">
					<a onclick="menuClick('index','home')" class="js-arrow" href="#"><i class="fas fa-home"></i>Home</a>
				</li>
				<li class="has-sub" onclick="changeArrow()">
					<a class="js-arrow" href="#">
						<i class="fas fa-file"></i>Treinamento&nbsp;
						<i id="arrow" class="fa fa-chevron-left j-dropdown"></i>
					</a>
					<ul class="list-unstyled navbar__sub-list js-sub-list">
						<li><a href="#" onclick="menuClick('td','home')">General</a></li>
						<li><a href="#" onclick="menuClick('td','mytrainings')">My Trainings</a></li>
						<li><a href="#" onclick="menuClick('td','newtraining')">Request a Training</a></li>
						<li><a href="#" onclick="menuClick('td','schedule')">TD Schedule</a></li>
						<li><a href="#" onclick="menuClick('td','points')">Pilot Support - Report</a></li>
						<?php /*<li><a href="#" onclick="menuclick('td','gcacheckout')">GCA</a></li> */ ?>
						<?php if ($member->vid == "379042" || $member->vid == "316499") {
            echo '<li><a href="#" onclick="menuClick(\'td\',\'ratingXfer\')">Rating Transfer</a></li>';
        } ?>
					</ul>
				</li>
			</ul>
		</div>
	</nav>
</header>
<?php
    }?>
