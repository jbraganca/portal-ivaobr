<?php 

$member = new member();
if(!$member->isConnected())
	$member->loginValidate();
else
	$member->getData();


/* DEFINE TREINAMENTO DISPONÍVEL */

$ratingatc = array("None","AS1","AS2","AS3","ADC","APC","ACC","SEC","SAI","CAI");
$ratingpilot = array("None","FS1","FS2","FS3","PP","SPP","CP","ATP","SFI","CFI");


if($_SESSION['lang'] == "PT")
{
	if($member->ratingatc < 8 && $member->ratingatc >=4)
		$training_rating[0] = $ratingatc[$member->ratingatc];
	else if($member->ratingatc < 4)
		$training_rating[0] = "Inicial ATC";
	else
		$training_rating[0] = "None";


	if($member->ratingpilot < 8 && $member->ratingpilot >=4)
		$training_rating[1] = $ratingpilot[$member->ratingpilot];
	else if($member->ratingpilot < 4)
		$training_rating[1] = "Inicial Piloto";
	else
		$training_rating[1] = "None";

?>

<div id="content">
	<div class="container-fluid">
		<div class="row-fluid">
<?php
	if($training_rating[0] == "None" && $training_rating[1] == "None"){
		echo '<div class="alert alert-danger">
      <h4 class="alert-heading">Oops!</h4><br>
	  Parece que você já tem rating acima de ACC e CP, certo? Infelizmente a divisão brasileira não tem treinamentos disponíveis para você neste momento.
			</div>';
	}
	else if($member->division != "BR")
	{
		echo '<div class="alert alert-danger">
		<h4 class="alert-heading">Oops!</h4><br>
		Parece que você é membro de outra divisão, certo? Infelizmente não há treinamentos disponíveis neste momento
		</div>';
	}
	else
	{
?>
<div class="col-lg-12">
	<div class="card">
		<div class="card-header">
			<strong>Solicitação de Treinamento</strong>
		</div>
		<div class="card-body card-block">
			<div>
				<div class="form-group">
					<label for="nf-email" class=" form-control-label">Nome</label>
					<input id="name" type="text" class="form-control" placeholder="First name" readonly="true" value="<?php echo $member->firstname." ".$member->lastname; ?>"/>
				</div>
				<div class="form-group">
					<label for="nf-password" class=" form-control-label">VID</label>
					<input id="vid" type="text" class="form-control" readonly="true" value="<?php echo $member->vid;?>" />
				</div>
				<div class="form-group">
					<label for="nf-password" class=" form-control-label">Seus Ratings</label><br>
					<img src="https://www.ivao.aero/data/images/ratings/pilot/<?php echo $member->ratingpilot;?>.gif" />
					<img src="https://www.ivao.aero/data/images/ratings/atc/<?php echo $member->ratingatc;?>.gif" />
				</div>
				
				<div class="form-group">
					<label for="select" class=" form-control-label">Selecione o rating</label>
					
					<select id="training" class="form-control">
					<?php foreach($training_rating as $training)if($training != "None")echo "<option>$training</option>";?>
					</select>
				</div>
				
				
				<div class="form-group">
					<label for="nf-email" class="form-control-label">Email</label>
					<input type="email" id="email" name="nf-email" placeholder="Insira um e-mail válido" class="form-control">
					<hr>
				</div>
				
				<div class="form-group">
					<label class="form-control-label">Disponibilidade</label>
						<span class="help-block">Selecione três datas que você tem disponibilidade</span> 
						
						<div class="row">
							<div class="col-lg-6">
								<label for="select" class="form-control-label">Data</label>
								<input id="date1" type="date" placeholder="Data" class="form-control" />
							</div>
							<div class="col-lg-6">
								<label for="select" class="form-control-label">Período</label>
								<select class="form-control" id="option1"><option>Manhã</option><option>Tarde</option><option>Noite</option></select>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-6">
								<label for="select" class="form-control-label">Data</label>
								<input id="date2" type="date" placeholder="Data" class="form-control" />
							</div>
							<div class="col-lg-6">
								<label for="select" class="form-control-label">Período</label>
								<select class="form-control" id="option2"><option>Manhã</option><option>Tarde</option><option>Noite</option></select>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-6">
								<label for="select" class="form-control-label">Data</label>
								<input id="date3" type="date" placeholder="Data" class="form-control" />
							</div>
							<div class="col-lg-6">
								<label for="select" class="form-control-label">Período</label>
								<select class="form-control" id="option3"><option>Manhã</option><option>Tarde</option><option>Noite</option></select>
							</div>
						</div>							
				</div>
				<div class="form-group">
					<button type="submit" onclick="formSend()" class="btn btn-primary btn-sm">
						<i class="fa fa-dot-circle-o"></i>  Solicitar
					</button>
				</div>	
				<div class="form-group">
					<label for="div" class="form-control-label">Política de Treinamento</label>
					<div class="controls">
						<span class="help-block">Ao clicar no botão <b>Solicitar</b> você concorda com a seguinte <b><a onclick="showTrainingPolicy()" href="#">política de treinamento</a></b></span> 
					</div>
					<div style="display:none" class="controls" id="trainingPolicy">
						1. São elegíveis para treinamentos os membros que atendam os seguites requisitos:<br><br>
						a. Ser membro da divisão brasileira;<br>
						b. Ser capaz de receber o treinamento na língua portuguesa;<br>
						c. Ter sido aprovado em exame teórico do rating solicitado;<br>
						d. Não ter solicitado o exame prático do rating solicitado;<br><br>
						NOTA: Os requisitos previstos no item c e d não se aplicam para membros solicitantes de treinamentos introdutórios a rede IVAO, modalidade: Inicial ATC e Inicial Piloto.<br><br>
						2. A disponibilidade de treinamentos está sujeita aos ratings possuídos pelos atuais membros do departamento de treinamento (IVAO Examiners e IVAO Traines);<br><br>
						3. As datas presentes neste formulário servem de parâmetro para melhor distribuição dos instrutores da divisão.<br><br>
						4. Caso o membro não possa comparecer ao treinamento previamente marcado, deverá comunicar o instrutor com antecedência mínima de 24 horas estando sujeito ao remarcamento do seu treinamento em conformidade com a agenda do instrutor. Em caso de atraso superior a 15 minutos ou comunicação do não comparecimento no prazo inferior a 24 horas, o treinamento será cancelado.<br>
					</div>
				</div>	
			</div>
		</div>
		<div class="card-footer">
			<?php /*<button type="submit" onclick="formSend()" class="btn btn-primary btn-sm">
				<i class="fa fa-dot-circle-o"></i>  Solicitar
			</button>*/?>
		</div>
	</div>
</div>
<script>
function showTrainingPolicy()
{
	$
	(
		function()
		{
			$("#trainingPolicy").show("slow");
		}
	);
}
function formSend()
{
	$
	(
		function()
		{
			var data = {_function: "newtraining",name: $("#name").val(), vid: $("#vid").val(), training: $("#training").val(), email: $("#email").val(),date1: $("#date1").val(),date2: $("#date2").val(),date3 :$("#date3").val(),option1:$("#option1").val(),option2:$("#option2").val(),option3:$("#option3").val()};
			$.post
			(
				"pages/functions/td.php",
				{
					data: data
				},
				function(resultado)
				{
					if(resultado == "disP")
						alert("Preencha a disponibilidade");
					else if(resultado == "emailP")
						alert("Preencha o seu e-mail");
					else if(resultado == "success")
					{
						menuClick("td","mytrainings");
					}
					else if(resultado=="TrainingRepetido")
					{
						alert("Você já solicitou este treinamento");
						menuClick("td","mytrainings");
					}
					else if(resultado == "timeE")
					{
						alert("Você está solicitando datas anteriores ao dia de hoje");
					}
					else
						alert(resultado);
				}
			);
		}
	);
}
</script>
<?php	
	}
} else if($_SESSION['lang'] == "EN")
{
	if($member->ratingatc < 7 && $member->ratingatc >=4)
		$training_rating[0] = $ratingatc[$member->ratingatc];
	else if($member->ratingatc < 4)
		$training_rating[0] = "Inicial ATC";
	else
		$training_rating[0] = "None";


	if($member->ratingpilot < 8 && $member->ratingpilot >=4)
		$training_rating[1] = $ratingpilot[$member->ratingpilot];
	else if($member->ratingpilot < 4)
		$training_rating[1] = "Inicial Piloto";
	else
		$training_rating[1] = "None";

?>

<div id="content">
	<div class="container-fluid">
		<div class="row-fluid">
<?php
	if($training_rating[0] == "None" && $training_rating[1] == "None"){
		echo '<div class="alert alert-danger">
      <h4 class="alert-heading">Oops!</h4><br>
	  Apparently you are already ACC and CP, right? Unfortunately we do not have any training for you in this moment.
			</div>';
	}
	else if($member->division != "BR")
	{
		echo '<div class="alert alert-danger">
		<h4 class="alert-heading">Oops!</h4><br>
		Apparently you are member of other division, all right? Unfortunately we do not have any training for you in this moment.
		</div>';
	}
	else
	{
?>
<div class="col-lg-12">
	<div class="card">
		<div class="card-header">
			<strong>Training Request</strong>
		</div>
		<div class="card-body card-block">
			<div>
				<div class="form-group">
					<label for="nf-email" class=" form-control-label">Name</label>
					<input id="name" type="text" class="form-control" placeholder="First name" readonly="true" value="<?php echo $member->firstname." ".$member->lastname; ?>"/>
				</div>
				<div class="form-group">
					<label for="nf-password" class=" form-control-label">VID</label>
					<input id="vid" type="text" class="form-control" readonly="true" value="<?php echo $member->vid;?>" />
				</div>
				<div class="form-group">
					<label for="nf-password" class=" form-control-label">Your Ratings</label><br>
					<img src="https://www.ivao.aero/data/images/ratings/pilot/<?php echo $member->ratingpilot;?>.gif" />
					<img src="https://www.ivao.aero/data/images/ratings/atc/<?php echo $member->ratingatc;?>.gif" />
				</div>
				
				<div class="form-group">
					<label for="select" class=" form-control-label">Select the rating</label>
					
					<select id="training" class="form-control">
					<?php foreach($training_rating as $training)if($training != "None")echo "<option>$training</option>";?>
					</select>
				</div>
				
				
				<div class="form-group">
					<label for="nf-email" class="form-control-label">Contact Email</label>
					<input type="email" id="email" name="nf-email" placeholder="Insert a valid email address" class="form-control">
					<hr>
				</div>
				
				<div class="form-group">
					<label class="form-control-label">Availability</label>
						<span class="help-block">Select 3 dates that you are available</span> 
						
						<div class="row">
							<div class="col-lg-6">
								<label for="select" class="form-control-label">Date</label>
								<input id="date1" type="date" placeholder="Data" class="form-control" />
							</div>
							<div class="col-lg-6">
								<label for="select" class="form-control-label">Period</label>
								<select class="form-control" id="option1"><option>Morning</option><option>Afternoon</option><option>Night</option></select>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-6">
								<label for="select" class="form-control-label">Date</label>
								<input id="date2" type="date" placeholder="Data" class="form-control" />
							</div>
							<div class="col-lg-6">
								<label for="select" class="form-control-label">Period</label>
								<select class="form-control" id="option2"><option>Morning</option><option>Afternoon</option><option>Night</option></select>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-6">
								<label for="select" class="form-control-label">Date</label>
								<input id="date3" type="date" placeholder="Data" class="form-control" />
							</div>
							<div class="col-lg-6">
								<label for="select" class="form-control-label">Period</label>
								<select class="form-control" id="option3"><option>Morning</option><option>Afternoon</option><option>Night</option></select>
							</div>
						</div>							
				</div>
				<div class="form-group">
					<button type="submit" onclick="formSend()" class="btn btn-primary btn-sm">
						<i class="fa fa-dot-circle-o"></i>  Solicitar
					</button>
				</div>	
				<div class="form-group">
					<label for="div" class="form-control-label">Traning Policy</label>
					<div class="controls">
						<span class="help-block">When click on <b>Request</b> button you are accepting the following <b><a onclick="showTrainingPolicy()" href="#">Traning Policy</a></b></span> 
					</div>
					<div style="display:none" class="controls" id="trainingPolicy">
								1. Training sessions are available for members attending the following requirements:<br><br>
								a. Trainee must be member of IVAO Brazilian division;<br>
								b. Trainee must be able to receive training on portuguese language;<br>
								c. Trainee must be approved in the theoretical exam of requested rating;<br>
								d. Trainee must not request practical exam of requested rating;<br><br>
								Note: The requirements in item c and d is not applied for members requesting introduction trainings to IVAO Network, modality: Initial ATC and Initial Pilot;<br><br>
								2. The availability of trainings is subject to ratings of Training Department members (IVAO Examiners and IVAO Trainers);<br><br>
								3. The dates on this form is used as parameter to provided the best balance of trainings and instructors of division;<br><br>
								4. If the member is unable to attend the previously schedule training, He need to inform their instructor at least 24 hours of advance subject to rescheduling of their training according instructor availability. In case of a delay above 15 minutes or communication of non-attendance within less than 24 hours, the training will be canceled.<br>
					</div>
				</div>	
			</div>
		</div>
		<div class="card-footer">
			<?php /*<button type="submit" onclick="formSend()" class="btn btn-primary btn-sm">
				<i class="fa fa-dot-circle-o"></i>  Request
			</button>*/?>
		</div>
	</div>
</div>
<script>
function showTrainingPolicy()
{
	$
	(
		function()
		{
			$("#trainingPolicy").show("slow");
		}
	);
}
function formSend()
{
	$
	(
		function()
		{
			var data = {_function: "newtraining",name: $("#name").val(), vid: $("#vid").val(), training: $("#training").val(), email: $("#email").val(),date1: $("#date1").val(),date2: $("#date2").val(),date3 :$("#date3").val(),option1:$("#option1").val(),option2:$("#option2").val(),option3:$("#option3").val()};
			$.post
			(
				"pages/functions/td.php",
				{
					data: data
				},
				function(resultado)
				{
					if(resultado == "disP")
						alert("Fill your availability");
					else if(resultado == "emailP")
						alert("Fill your e-mail");
					else if(resultado == "success")
					{
						menuClick("td","mytrainings");
					}
					else if(resultado=="TrainingRepetido")
					{
						alert("You already requested this training");
						menuClick("td","mytrainings");
					}
					else if(resultado == "timeE")
					{
						alert("You are requesting training for a before today's date");
					}
					else
						alert(resultado);
				}
			);
		}
	);
}
</script>
<?php	
	}
}