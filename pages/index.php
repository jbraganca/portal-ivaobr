<?php
include("../classes.php");
$member = new member();
$member->getdata();

$subpage = $_GET["subpage"];

$vid = $member->vid;
$name = $member->firstname . " " . $member->lastname;

$page = "index-$subpage";

if (file_exists("subpage/index-$subpage.php")) {
    require("subpage/index-$subpage.php");
} else {
    require("subpage/404.php");
}
