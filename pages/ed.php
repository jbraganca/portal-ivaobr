<?php 
include("../classes.php");
$member = new member();
$member->getdata();

$subpage = $_GET["subpage"];

$vid = $member->vid;
$name = $member->firstname . " " . $member->lastname;

$page = "ed-$subpage";

$string = "$name - $vid - $page - ".$_SESSION['lang'];

$ponteiro = fopen("pages.txt","a");
fwrite($ponteiro, $string."\n");

if(file_exists("subpage/ed-$subpage.php"))
	require("subpage/ed-$subpage.php");
else
	require("subpage/404.php");

?>

