<style type="text/css">
.j-dropdown {
	float: right;
	padding: 8px 0px 0px 16px;
}
</style>
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>

<?php
//$_SESSION['lang'] = "PT";
if ($_SESSION['lang'] == "PT") {
    ?>

<aside class="menu-sidebar d-none d-lg-block">
	<div class="logo">
		<a href="#">
			<img src="images/icon/logo.png" width="70%"/>
		</a>
	</div>
	<div class="menu-sidebar__content js-scrollbar1">
		<nav class="navbar-sidebar">
			<ul class="list-unstyled navbar__list">
				<li class="active has-sub">
					<a onclick="menuClick('index','home')" class="js-arrow" href="#">
						<i class="fas fa-home"></i>Início
					</a>
				</li>
				<li class="has-sub" onclick="changeArrow('TD')">
					<a class="js-arrow" href="#">
						<i class="fas fa-file"></i>Treinamento&nbsp;
						<i id="arrowTD" class="fa fa-chevron-left j-dropdown"></i>
					</a>
					<ul class="list-unstyled navbar__sub-list js-sub-list">
						<li><a href="#" onclick="menuClick('td','home')">Geral</a></li>
						<li><a href="#" onclick="menuClick('td','mytrainings')">Meus Treinamentos</a></li>
						<li><a href="#" onclick="menuClick('td','newtraining')">Solicitar Treinamento</a></li>
						<li><a href="#" onclick="menuClick('td','schedule')">Agenda TD</a></li>
						<li><a href="#" onclick="menuClick('td','points')">Reporte - Pilot Support</a></li>
						<?php /*<li><a href="#" onclick="menuclick('td','gcacheckout')">GCA</a></li> */ ?>
						<?php if ($member->vid == "379042" || $member->vid == "316499" || $member->vid == "325034") {
        echo '<li><a href="#" onclick="menuClick(\'td\',\'ratingXfer\')">Transferir rating</a></li>';
    } ?>
					</ul>
				</li>
				<?php if ($member->vid == "325034") {
        ?>
				<li class="has-sub" onclick="changeArrow('ED')">
					<a class="js-arrow" href="#">
						<i class="fas fa-file"></i>Eventos&nbsp;
						<i id="arrowED" class="fa fa-chevron-left j-dropdown"></i>
					</a>
					<ul class="list-unstyled navbar__sub-list js-sub-list">
						<li><a href="#" onclick="menuClick('ed','home')">Geral</a></li>
						<li><a href="#" onclick="menuClick('ed','myevents')">Meus Eventos</a></li>
					</ul>
				</li>
				<?php
    } ?>
			</ul>
		</nav>
	</div>
</aside>

<?php
} elseif ($_SESSION['lang'] == "EN") {
        ?>

<aside class="menu-sidebar d-none d-lg-block">
	<div class="logo">
		<a href="#">
			<img src="images/icon/logo.png" width="70%"/>
		</a>
	</div>
	<div class="menu-sidebar__content js-scrollbar1">
		<nav class="navbar-sidebar">
			<ul class="list-unstyled navbar__list">
				<li class="active has-sub">
					<a onclick="menuClick('index','home')" class="js-arrow" href="#">
						<i class="fas fa-home"></i>Home
					</a>
				</li>
				<li class="has-sub" onclick="changeArrow()">
					<a class="js-arrow" href="#">
						<i class="fas fa-file"></i>Training&nbsp;
						<i id="arrow" class="fa fa-chevron-left j-dropdown"></i>
					</a>
					<ul class="list-unstyled navbar__sub-list js-sub-list">
						<li><a href="#" onclick="menuClick('td','home')">General</a></li>
						<li><a href="#" onclick="menuClick('td','mytrainings')">My Trainings</a></li>
						<li><a href="#" onclick="menuClick('td','newtraining')">Request a Training</a></li>
						<li><a href="#" onclick="menuClick('td','schedule')">TD Schedule</a></li>
						<li><a href="#" onclick="menuClick('td','points')">Pilot Support - Report</a></li>
						<?php /*<li><a href="#" onclick="menuclick('td','gcacheckout')">GCA</a></li> */ ?>
						<?php if ($member->vid == "379042" || $member->vid == "316499" || $member->vid == "325034") {
            echo '<li><a href="#" onclick="menuClick(\'td\',\'ratingXfer\')">Rating Transfer</a></li>';
        } ?>
					</ul>
				</li>
			</ul>
		</nav>
	</div>
</aside>

<?php
    }
?>



<script>
function menuClick(page, subpage)
{
	$
	(
		function()
		{
			$(".menu").attr("class","menu");
			$("#"+page).attr("class","menu active");
			page = "pages/"+page+".php?subpage="+subpage;
			//alert(page);
			$.post(page,function(resultado){$("#content").html(resultado)});
		}
	);
}
var arrowState = true;
function changeArrow(item){
	if (arrowState){
		$("#arrow"+item).hide().removeClass("fa-chevron-left").fadeIn().addClass("fa-chevron-down");
		arrowState = false;
	} else {
		$("#arrow"+item).hide().removeClass("fa-chevron-down").fadeIn().addClass("fa-chevron-left");
		arrowState = true;
	}

}
</script>

<script src="js/jquery.min.js"></script>
<script src="js/jquery.ui.custom.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/matrix.js"></script>
