<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
 
<?php 

$member = new member();
if(!$member->isConnected())
	$member->loginValidate();
else
	$member->getData();

$return = $member->getEventData();	

//echo "<pre>";print_r($return);echo "</pre>";

$ratings = array("None","None","AS1","AS2","AS3","ADC","APC","ACC","SEC");

?>



<?php if($_SESSION['lang'] == "PT"){?>

<div id="container" class="container-fluid">
	<div class="row">
		<div id="pagina" class="col-lg-12">
			<div class="card">
				<div class="card-header">
					<i class="mr-2 fa fa-align-justify"></i>
					<strong class="card-title" v-if="headerText">Participações Eventos</strong>
				</div>
				 <div class="card-body">
					<table class="table table-borderless table-striped table-earning">
					  <thead>
						<tr>
						  <th style="text-align:center">Callsign</th>
						  <th style="text-align:center">Evento</th>
						  <th style="text-align:center">Data</th>
						  <th style="text-align:center">Tempo Online</th>
						</tr>
					  </thead>
					  <tbody>
						  <?php $i=0;foreach($return as $event) {$today = date("U", strtotime('-1 day'));?>
						  <tr>
							<td style="text-align:center"><?php echo $event['callsign'];?></td>
							<td style="text-align:center"><?php echo $event['event'];?></td>
							<td style="text-align:center"><?php echo date("d/m/Y",$event['dataStart']);?></td>
							<td style="text-align:center"><?php echo floor($event['totalTime'])."h".str_pad(($event['totalTime']-floor($event['totalTime']))*60,2,"0",STR_PAD_LEFT);?></td>
						  </tr>
						  <?php $i++;} ?>
					  </tbody>
					</table>
				</div>		 
			</div>
		</div>
	</div>
</div>
<?php } else {?>

<div id="container" class="container-fluid">
	<div class="row">
		<div id="pagina" class="col-lg-12">
			<div class="card">
				<div class="card-header">
					<i class="mr-2 fa fa-align-justify"></i>
					<strong class="card-title" v-if="headerText">ATC Exams/Trainings Schedules</strong>
				</div>
				 <div class="card-body">
					<table class="table table-borderless table-striped table-earning">
					  <thead>
						<tr>
						  <th style="text-align:center">Rating</th>
						  <th style="text-align:center">Local</th>
						  <th style="text-align:center">Date</th>
						  <th style="text-align:center">Time (Z)</th>
						</tr>
					  </thead>
					  <tbody>
						  <?php $i=0;foreach($return as $event) {$today = date("U", strtotime('-1 day'));?>
						  <tr>
							<?php if(($event['rating'] == "ADC" || $event['rating'] == "APC" || $event['rating'] == "ACC" || $event['rating'] == "SEC") && ($today<$event['data'])){?>
							<td style="text-align:center"><img src="https://www.ivao.aero/data/images/ratings/atc/<?php echo array_search($event['rating'],$ratings);?>.gif"/></td>
							<td style="text-align:center"><?php echo $event['local'];?></td>
							<td style="text-align:center"><?php echo date("d/m/Y",$event['data']);?></td>
							<td style="text-align:center"><?php echo $event['time'];?></td>
							<?php }?>
						  </tr>
						  <?php $i++;} ?>
					  </tbody>
					</table>
				</div>		 
			</div>
		</div>
	</div>
</div>

<?php }?>
