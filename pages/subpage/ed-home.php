<?php 
$dir = $_SERVER['DOCUMENT_ROOT'];
require_once "$dir/includes/functions/functions.php";
require_once "$dir/includes/config/config.php";

$member = new member();
if(!$member->isConnected())
	$member->loginValidate();
else
	$member->getData();

$division = new division();
$division->db = $member->db;

$notams = $division->getNOTAMs();




$stats = $division->getEventsStats();

//print_r($stats);

if($_SESSION['lang'] == "PT"){?>

<div class="section__content section__content--p30">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="overview-wrap">
					<h2 class="title-1">Bem vindo ao Departamento de Eventos, <?php echo $member->firstname;?>!</h2>
				</div>
			</div>
		</div>
		<hr>
		<section class="statistic statistic2">
			<div class="row">
				<div class="col-md-6 col-lg-3">
					<div class="statistic__item bg-danger">
						<span class="desc">Conhecimento Avançado</span><hr>
						<h2 class="number"><?php echo $stats[0];?> treinamentos de ratings</h2>
						<span class="desc">efetuados com sucesso</span>
						<div class="icon">
							<i class="fa fa-book"></i>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-3">
					<div class="statistic__item bg-secondary">
						<span class="desc">Conhecimento básico</span><hr>
						<h2 class="number"><?php echo $stats[1];?> Treinamentos introdutórios</h2>
						<span class="desc">efetuados com sucesso</span>
						<div class="icon">
							<i class="fa fa-user"></i>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-3">
					<div class="statistic__item bg-success">
						<span class="desc">Treinamentos ATC</span><hr>
						<h2 class="number"><?php echo $stats[2];?> Controladores Virtuais</h2>
						<span class="desc">devidamente instruídos</span>
						<div class="icon">
							<i class="fa fa-globe"></i>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-3">
					<div class="statistic__item bg-info">
						<span class="desc">Treinamentos Pilotos</span><hr>
						<h2 class="number"><?php echo $stats[3]?> Pilotos <br>Virtuais</h2>
						<span class="desc">devidamente instruídos</span>
						<div class="icon">
							<i class="fa fa-fighter-jet"></i>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<div class="row">
			<div class="col-md-12">
				<div class="copyright">
					<p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
				</div>
			</div>
		</div>
	</div>
</div>
                      

<?php } else if($_SESSION['lang'] == "EN"){?>

<div class="section__content section__content--p30">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="overview-wrap">
					<h2 class="title-1">Welcome to Events Department, <?php echo $member->firstname;?>!</h2>
				</div>
			</div>
		</div>
		<hr>
		<section class="statistic statistic2">
			<div class="row">
				<div class="col-md-6 col-lg-3">
					<div class="statistic__item bg-danger">
						<span class="desc">Advanced Knowledge</span><hr>
						<h2 class="number"><?php echo $stats[0];?> Rating Trainings</h2>
						<span class="desc">successful completed</span>
						<div class="icon">
							<i class="fa fa-book"></i>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-3">
					<div class="statistic__item bg-secondary">
						<span class="desc">Basic Knowledge</span><hr>
						<h2 class="number"><?php echo $stats[1];?> Introductory Trainings</h2>
						<span class="desc">successful completed</span>
						<div class="icon">
							<i class="fa fa-user"></i>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-3">
					<div class="statistic__item bg-success">
						<span class="desc">ATC Trainings</span><hr>
						<h2 class="number"><?php echo $stats[2];?> Controllers</h2>
						<span class="desc">successful instructed</span>
						<div class="icon">
							<i class="fa fa-globe"></i>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-3">
					<div class="statistic__item bg-info">
						<span class="desc">Pilot Trainings</span><hr>
						<h2 class="number"><?php echo $stats[3]?> Pilots</h2>
						<span class="desc">successful instructed</span>
						<div class="icon">
							<i class="fa fa-fighter-jet"></i>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<div class="row">
			<div class="col-md-12">
				<div class="copyright">
					<p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>


<script>
function mostrarNotam(id)
{
	$
	(
		function()
		{
			$.post
			(
				"pages/functions/home.php",
				{
					request: "getNOTAMs",
					data: id
				},
				function (resultado)
				{
					notam = jQuery.parseJSON(resultado);
					<?php if($_SESSION['lang'] == "PT"){?>
					$("#titulo_notam").html(notam.titulo_pt.replace("[NOTAM] ",""));
					$("#conteudo_notam").html(notam.conteudo_pt);<?php } else if($_SESSION['lang'] == "EN"){?>
					$("#titulo_notam").html(notam.titulo_en.replace("[NOTAM] ",""));
					$("#conteudo_notam").html(notam.conteudo_en);
					<?php }?>
					$("#notams").hide();
					$("#notam").show();
				}
			);
		}
	);
}

function hideNotams()
{
	$
	(
		function()
		{
			$("#notam").hide();
			$("#notams").show();
		}
	);
}

</script>