<?php 

$dir = $_SERVER['DOCUMENT_ROOT'];
require_once "$dir/includes/functions/functions.php";
require_once "$dir/includes/config/config.php";

$member = new member();
if(!$member->isConnected())
	$member->loginValidate();
else
	$member->getData();

$division = new division();
$division->db = $member->db;

$notams = $division->getNOTAMs();




$stats = dashboard_stats();

//print_r($stats);

if($_SESSION['lang'] == "PT"){?>

<div class="section__content section__content--p30">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="overview-wrap">
					<h2 class="title-1">Bem vindo, <?php echo $member->firstname;?>!</h2>
				</div>
			</div>
		</div>
		<hr>
		<section class="statistic statistic2">
			<div class="row">
				<div class="col-md-6 col-lg-3">
					<div class="statistic__item statistic__item--green">
						<span class="desc">Rotas de/para SB**</span><hr>
						<h2 class="number"><?php echo $stats[1];?></h2>
						<span class="desc">voos online</span>
						<div class="icon">
							<i class="fa fa-plane"></i>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-3">
					<div class="statistic__item statistic__item--orange">
						<span class="desc">Posições ATC Brasileiras</span><hr>
						<h2 class="number"><?php echo $stats[2];?></h2>
						<span class="desc">ATC Online</span>
						<div class="icon">
							<i class="fa fa-globe"></i>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-3">
					<div class="statistic__item statistic__item--blue">
						<span class="desc">Maior número de partidas</span><hr>
						<h2 class="number"><?php echo $stats[3]." - ".$stats[4];?></h2>
						<div class="icon">
							<i class="fa fa-arrow-up"></i>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-3">
					<div class="statistic__item statistic__item--red">
						<span class="desc">Maior número de chegadas</span><hr>
						<h2 class="number"><?php echo $stats[5]." - ".$stats[6];?></h2>
						<div class="icon">
							<i class="fa fa-arrow-down"></i>
						</div>
					</div>
				</div>
			</div>
		</section>
		<div class="row">
			<div class="col-lg-12">
				<div class="au-card au-card--no-shadow au-card--no-pad m-b-40">
					<div class="au-card-title" style="background-image:url('images/bg-title-02.jpg');">
						<div class="bg-overlay bg-overlay--blue"></div>
						<h3>
							<i class="zmdi zmdi-comment-text"></i>NOTAMs</h3>
						<button onclick="hideNotams()" class="au-btn-plus">
							<i class="zmdi zmdi-close"></i>
						</button>
					</div>
					<div id="notambox" class="au-inbox-wrap js-inbox-wrap">
						<div id="notams" class="au-message js-list-load">
							<div class="au-message__noti">
								<p>Últimos NOTAMs</p>
							</div>
							<div class="au-message-list">
							<?php for($i=0;$i<sizeof($notams);$i++){?>
								<div style="cursor:pointer">
									<div onclick="mostrarNotam(<?php echo $notams[$i]['id'];?>)" class="au-message__item-inner">
										<div class="au-message__item-text">
											<div class="text">
												<h5 class="name"><?php echo $notams[$i]['nome_autor'];?></h5>
												<p><?php echo str_replace("[NOTAM] ","",$notams[$i]['titulo_pt']);?></p>
											</div>
										</div>
										<div class="au-message__item-time">
											<span><?php echo date("d - M - Y",strtotime($notams[$i]['data']));?></span>
										</div>
									</div>
								</div>
							<?}?>
							</div>

						</div>
						<div style="display:none" id="notam" class="au-chat">
							<div class="au-chat__title">
								<div class="au-chat-info">
									<span class="nick">
										<a href="#" id="titulo_notam"/>
									</span>
								</div>
							</div>
							<div id="conteudo_notam" class="au-chat__content">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="copyright">
					<p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
				</div>
			</div>
		</div>
	</div>
</div>
                      

<?php } else if($_SESSION['lang'] == "EN"){?>

<div class="section__content section__content--p30">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="overview-wrap">
					<h2 class="title-1">Welcome, <?php echo $member->firstname;?>!</h2>
				</div>
			</div>
		</div>
		<hr>
		<section class="statistic statistic2">
			<div class="row">
				<div class="col-md-6 col-lg-3">
					<div class="statistic__item statistic__item--green">
						<span class="desc">Routes to/from SB**</span><hr>
						<h2 class="number"><?php echo $stats[1];?></h2>
						<span class="desc">online flights</span>
						<div class="icon">
							<i class="fa fa-plane"></i>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-3">
					<div class="statistic__item statistic__item--orange">
						<span class="desc">Brazilian ATC positions</span><hr>
						<h2 class="number"><?php echo $stats[2];?></h2>
						<span class="desc">Online ATCs<span>
						<div class="icon">
							<i class="fa fa-globe"></i>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-3">
					<div class="statistic__item statistic__item--blue">
						<span class="desc">Highest number of departures</span><hr>
						<h2 class="number"><?php echo $stats[3]." - ".$stats[4];?></h2>
						<div class="icon">
							<i class="fa fa-arrow-up"></i>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-3">
					<div class="statistic__item statistic__item--red">
						<span class="desc">Highest number of arrivals</span><hr>
						<h2 class="number"><?php echo $stats[5]." - ".$stats[6];?></h2>
						<div class="icon">
							<i class="fa fa-arrow-down"></i>
						</div>
					</div>
				</div>
			</div>
		</section>
		<div class="row">
			<div class="col-lg-12">
				<div class="au-card au-card--no-shadow au-card--no-pad m-b-40">
					<div class="au-card-title" style="background-image:url('images/bg-title-02.jpg');">
						<div class="bg-overlay bg-overlay--blue"></div>
						<h3>
							<i class="zmdi zmdi-comment-text"></i>NOTAMs</h3>
						<button onclick="hideNotams()" class="au-btn-plus">
							<i class="zmdi zmdi-close"></i>
						</button>
					</div>
					<div id="notambox" class="au-inbox-wrap js-inbox-wrap">
						<div id="notams" class="au-message js-list-load">
							<div class="au-message__noti">
								<p>Last NOTAMs</p>
							</div>
							<div class="au-message-list">
							<?php for($i=0;$i<sizeof($notams);$i++){?>
								<div style="cursor:pointer">
									<div onclick="mostrarNotam(<?php echo $notams[$i]['id'];?>)" class="au-message__item-inner">
										<div class="au-message__item-text">
											<div class="text">
												<h5 class="name"><?php echo $notams[$i]['nome_autor'];?></h5>
												<p><?php echo str_replace("[NOTAM] ","",$notams[$i]['titulo_en']);?></p>
											</div>
										</div>
										<div class="au-message__item-time">
											<span><?php echo date("d - M - Y",strtotime($notams[$i]['data']));?></span>
										</div>
									</div>
								</div>
							<?}?>
							</div>

						</div>
						<div style="display:none" id="notam" class="au-chat">
							<div class="au-chat__title">
								<div class="au-chat-info">
									<span class="nick">
										<a href="#" id="titulo_notam"/>
									</span>
								</div>
							</div>
							<div id="conteudo_notam" class="au-chat__content">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="copyright">
					<p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>


<script>
function mostrarNotam(id)
{
	$
	(
		function()
		{
			$.post
			(
				"pages/functions/home.php",
				{
					request: "getNOTAMs",
					data: id
				},
				function (resultado)
				{
					notam = jQuery.parseJSON(resultado);
					<?php if($_SESSION['lang'] == "PT"){?>
					$("#titulo_notam").html(notam.titulo_pt.replace("[NOTAM] ",""));
					$("#conteudo_notam").html(notam.conteudo_pt);<?php } else if($_SESSION['lang'] == "EN"){?>
					$("#titulo_notam").html(notam.titulo_en.replace("[NOTAM] ",""));
					$("#conteudo_notam").html(notam.conteudo_en);
					<?php }?>
					$("#notams").hide();
					$("#notam").show();
				}
			);
		}
	);
}

function hideNotams()
{
	$
	(
		function()
		{
			$("#notam").hide();
			$("#notams").show();
		}
	);
}

</script>